# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Contains the partial code for swipable cards like Google primer

### App contains the following features ###

* Retrofit with RXJava used for networking and added the custom converter factory to parse the response starts with /
* Used MVVM architecture
* Used Koin for dependency injection
* Swipable cards achieved with RecyclerView with IndefinitePagerIndicator for page indication


### What is pending? ###
* Adding the MotionLayout with recyclerview and respective motionListener in adapter class  to match the Google Primer view 

### What could be improved? ###
* Converting the RXJava module to Coroutines 
* Converting the MVVM architecture to Clean architecture
