package com.projects.basis.repositories

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.projects.basis.dtos.ContentResponseDTO
import com.projects.basis.network.ApiComponent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Yashaswi NP on 28/02/21.
 */

class ContentRepository {

    private var disposable: Disposable? = null
    private val apiComponent = ApiComponent


    /**
     * fetches data from API
     */
    fun fetchContent(): MutableLiveData<ContentResponseDTO?> {
        val contentData = MutableLiveData<ContentResponseDTO?>()
        disposable = apiComponent.getApiInterface()
            .getContentData()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.isSuccessful) {
                    contentData.value = it.body()
                }
            }, {
                contentData.value = null
                Log.e("ContentRepo", it.message + it.stackTrace)
                //  disposable?.dispose()
            }, {
                disposable?.dispose()
            })
        return contentData
    }

}