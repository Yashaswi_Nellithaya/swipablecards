package com.projects.basis.di

import com.projects.basis.network.ApiComponent
import com.projects.basis.repositories.ContentRepository
import com.projects.basis.viewmodels.ContentViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Yashaswi N P on 21/5/20.
 * contains all the koin modules
 */
val applicationModule = module {
    single { ApiComponent }
    single { ContentRepository() }
    viewModel {
        ContentViewModel()
    }
}