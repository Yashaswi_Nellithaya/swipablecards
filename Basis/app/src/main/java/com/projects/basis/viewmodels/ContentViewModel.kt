package com.projects.basis.viewmodels

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.projects.basis.dtos.ContentDataDTO
import com.projects.basis.dtos.ContentResponseDTO
import com.projects.basis.repositories.ContentRepository

/**
 * Created by Yashaswi NP on 28/02/21.
 */


class ContentViewModel : ViewModel() {
    private var contentRepository = ContentRepository()
    var contentData : List<ContentDataDTO> = ArrayList()

    /**
     * returns the contents
     */
    fun getContentData(): LiveData<ContentResponseDTO?> {
        return contentRepository.fetchContent()
    }
}