package com.projects.basis.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.projects.basis.R
import com.projects.basis.dtos.ContentDataDTO
import kotlinx.android.synthetic.main.item_content_card.view.*

class ContentDataAdapter(context: Context) :
    RecyclerView.Adapter<ContentDataAdapter.ContentViewHolder>() {

    private val mContext: Context = context
    private var items: List<ContentDataDTO> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.item_content_card, parent, false)
        return ContentViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size ?: 0
    }

    override fun onBindViewHolder(holder: ContentViewHolder, position: Int) {
        if (items.isNotEmpty()) {
            val content = items[position]
            holder.contentTitle.text = content.text
        }
    }

    /**
     * to get the data from activities / fragments
     */
    fun setDataAndRefresh(contactsList: List<ContentDataDTO>?) {
        this.items = contactsList ?: ArrayList()
        notifyDataSetChanged()
    }

    inner class ContentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        val contentTitle: TextView = itemView.item_content_tv

        override fun onClick(v: View?) {
        }
    }
}
