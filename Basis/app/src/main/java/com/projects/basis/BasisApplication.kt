package com.projects.basis

import android.app.Application
import com.projects.basis.di.applicationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Yashaswi NP on 28/02/21.
 */

class BasisApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        appplicationInstance = this
        initKoin()

    }

    /**
     * initialises koin
     */
    private fun initKoin() {
        startKoin {
            androidContext(applicationContext)
            modules(listOf(applicationModule))
        }
    }

    companion object {
        var appplicationInstance: BasisApplication? = null
    }
}