package com.projects.basis.ui

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.projects.basis.R
import com.projects.basis.dtos.ContentDataDTO
import com.projects.basis.viewmodels.ContentViewModel
import kotlinx.android.synthetic.main.fragment_content.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ContentFragment : Fragment() {
    private var mContentData: List<ContentDataDTO>? = null
    private val mViewModel: ContentViewModel by sharedViewModel()

    companion object {
        private const val KEY_DATA = "DATA"
        fun newInstance(): ContentFragment {
            val args = Bundle()
            val fragment = ContentFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private var contentLayoutManager = object : LinearLayoutManager(context, HORIZONTAL, false) {
        override fun smoothScrollToPosition(
            recyclerView: RecyclerView,
            state: RecyclerView.State?,
            position: Int
        ) {
            val smoothScroller = object : LinearSmoothScroller(recyclerView.context) {
                private val SPEED = 300f// Change this value (default=25f)
                override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics): Float {
                    return SPEED / displayMetrics.densityDpi
                }
            }
            smoothScroller.targetPosition = position
            startSmoothScroll(smoothScroller)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_content, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    private fun initUI() {
        mContentData = mViewModel.contentData
        content_card_scrollerRV.layoutManager = contentLayoutManager
        content_card_scrollerRV.setHasFixedSize(true)
        val adapter = context?.let { ContentDataAdapter(it) }
        content_card_scrollerRV.adapter = adapter
        content_card_scrollerRV.onFlingListener = null
        val pagerSnapHelper = PagerSnapHelper()
        content_card_scroller_indicator.attachToRecyclerView(content_card_scrollerRV)
        pagerSnapHelper.attachToRecyclerView(content_card_scrollerRV)

        mContentData.let {
            (content_card_scrollerRV.adapter as ContentDataAdapter).setDataAndRefresh(it)
        }
        (content_card_scrollerRV.adapter as ContentDataAdapter).notifyDataSetChanged()
//            if (it.size <= 1) {
//                content_card_scrollerRV.gone()
//            } else {
//                content_card_scrollerRV.visible()
//            }
    }
}
