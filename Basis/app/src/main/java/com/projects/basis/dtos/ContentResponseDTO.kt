package com.projects.basis.dtos

import com.google.gson.annotations.SerializedName

data class ContentResponseDTO(
    @SerializedName("data")
    val data: List<ContentDataDTO>? = null
)

data class ContentDataDTO(
    @SerializedName("id")
    val id: String? = null,
    @SerializedName("text")
    val text: String? = null
)
