package com.projects.basis.network

import com.projects.basis.utils.AppConstants.BASE_URL
import com.projects.basis.utils.AppConstants.REQUEST_READ_TIME_OUT_IN_SECONDS
import com.projects.basis.utils.AppConstants.REQUEST_WRITE_TIME_OUT_IN_MINUTES
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.util.concurrent.TimeUnit


/**
 * Created by Yashaswi N P on 28/2/21
 */
object ApiComponent {

    /**
     * Configure Network Request client
     */
    fun getApiInterface(): ApiService {
        val httpClientBuilder = OkHttpClient.Builder()
            .readTimeout(REQUEST_READ_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS)
            .writeTimeout(REQUEST_WRITE_TIME_OUT_IN_MINUTES, TimeUnit.MINUTES)


        val interceptor = HttpLoggingInterceptor()
        interceptor.apply { interceptor.level = HttpLoggingInterceptor.Level.BODY }
        httpClientBuilder.interceptors().add(interceptor)

        val client = httpClientBuilder.build()
        return Retrofit.Builder()
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(CustomConverterFactory())
            .baseUrl(BASE_URL)
            .build()
            .create(ApiService::class.java)


    }

}


