package com.projects.basis.utils

/**
 * Created by Yashaswi N P on 28/2/21.
 */

object AppConstants {

    /* network related constants */
    const val REQUEST_READ_TIME_OUT_IN_SECONDS: Long = 30
    const val REQUEST_WRITE_TIME_OUT_IN_MINUTES: Long = 1
    const val BASE_URL = "https://gist.githubusercontent.com/"
    const val GET_CONTENT = "anishbajpai014/d482191cb4fff429333c5ec64b38c197/raw/b11f56c3177a9ddc6649288c80a004e7df41e3b9/HiringTask.json"

}