package com.projects.basis.network

import com.projects.basis.dtos.ContentResponseDTO
import com.projects.basis.utils.AppConstants
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

/**
 * Created by Yashaswi N P on 28/2/21.
 */

interface ApiService {

    // api endpoint to to get the content
    @GET(AppConstants.GET_CONTENT)
    fun getContentData(): Observable<Response<ContentResponseDTO>>
}